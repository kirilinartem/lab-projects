#define _USE_MATH_DEFINES
#include <iostream>
#include <conio.h>
#include <math.h>
#include <Windows.h>
#include "glut.h"
#include "GLAUX.H"
#include "Camera.h"
#include <vector>
using namespace std;

void changeSize(int width, int height); //�, ���������� ��� ��������� ����
void renderScene(); //�, ���������� ��� ��������� �����

double Func1(double x);

/*������� ��� ������� ����������� ������� ��������� x �� ������� [begin, end] ��� ��������� ��������*/
vector<double> CreatePMas(double begin, double end, int numofpoints = 100);

/*������� ��� ������� ����������� ������� ��������� xy ��� ��������� �������
������� Func �� ����������� x �� ������� Mas*/
vector<pair<double, double>> CreateFPMas(double (*Func)(double), vector<double> Mas);

/*������� ��� ����������� ����������� �������� ������� �� ���������� �� ������� Mas*/
vector<pair<double, double>> CreatePFPMas(vector<pair<double, double>> Mas);

/*������� ��� ��������� ������� �� ������ �� ������ �� ������� Mas. 
���� ������� ������� r,g,b. ������ size*/
void DrawGrapic(vector<pair<double, double>> Mas, double r, double g, double b, double size = 1);

/*������� ��� ��������� ������� ������� ���������� (����� � ����������� ��������, 
������� - �����������)*/
void DrawFullGrapic(vector<pair<double, double>> MasF, vector<pair<double, double>> MasPF, double size = 1);

/*�������, ���������� ���������� (������� ����������) � ��������� (�������� �������) ���������� ������� �� ������� Mas. 
���������� ���������� ����������� � ��������� epsilon.*/
void DrawExtremum(vector<pair<double, double>> Mas, double epsilon = 0.01);

/*������� ������������ ������� ���������*/
void DrawOSK();

/*������� ������������ �����*/
void DrawLine(float x1, float y1, float z1, float x2, float y2, float z2, float r, float g, float b, float size = 1.0);

double a, b, eps;

int main(int argc, char* argv[])
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    cout << "������ ������� ����������: ����� ������ ���������� ���������� ��������, ������� - �����������. "
        << "������� ���������� ���������� ���������� ���������� (� ��������� eps), �������� - ��������� ���������� �� ������� [a,b]. "
        << "������ ��������� ������ ����������� �������"
        << endl;
    cout << "������� ������� [a,b]:" << endl
         << "a="; cin >> a; 
    cout << "b="; cin >> b;
    cout << "������� eps:" << endl
        << "eps="; cin >> eps;
    cout << endl;

    glutInit(&argc, argv); //�������������� Glut
    glutInitWindowSize(800, 600); //������������� ������� ����
    glutInitWindowPosition(200, 200); //������������� ��������� ��� �������� ������ ���� ������
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE); //������������� ��� ���� RGB - ����� � ������� ������

    int window1 = glutCreateWindow("LR2"); //�������� ���� � ������ LR2 � ��������� ��� handler � window1

    glutReshapeFunc(changeSize); //���������� ������� changeSize, ��� ������� ��� ����������� ����������� ���� ��� ��������� ����
    glutDisplayFunc(renderScene); //���������� ������� renderScene, ��� ������� ��� ��������� ����������� ����
    glutIdleFunc(renderScene);

    C2DCamera::KeyboardMove();

    glutMainLoop(); //������ � ������� ���� GLUT

    return 0;
}

void changeSize(int width, int height)
{
    C2DCamera::DoGood(width, height, 0, 0, 0);
}

void renderScene()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    C2DCamera::Look();

    vector<double> MasX = CreatePMas(a, b, 1000);

    vector<pair<double, double>> MasF = CreateFPMas(Func1, MasX); //������ ��������� ����� ������� �������
    vector<pair<double, double>> MasPF = CreatePFPMas(MasF); //������ ��������� ����� ����������� ������� �������

    //DrawGrapic(MasF, 1, 0, 0);
    DrawFullGrapic(MasF, MasPF);
    DrawExtremum(MasF, eps);
    DrawGrapic(MasPF, 0, 1, 0);

    DrawOSK();

    glutSwapBuffers(); //�������� �������
}

double Func1(double x)
{
    return sin(x)+sin(3*x)+sin(6*x);
}

vector<double> CreatePMas(double begin, double end, int numofpoints)
{
    vector<double> buf;
    double dn = (end - begin) / double(numofpoints-1);
    for (double i = begin; i < end; i += dn)
    {
        buf.push_back(i);
    }
    buf.push_back(end);
    return buf;
}

vector<pair<double,double>> CreateFPMas(double (*Func)(double), vector<double> Mas)
{
    vector<pair<double, double>> buf;
    for (double i : Mas)
    {
        buf.push_back(make_pair(i,Func(i)));
    }
    return buf;
}

vector<pair<double, double>> CreatePFPMas(vector<pair<double, double>> Mas)
{
    vector<pair<double, double>> buf;
    int n = Mas.size();
    for (int i = 0; i < n; i++)
    {
        if (i != n - 1)
            buf.push_back(make_pair(Mas[i].first, (Mas[i + 1].second - Mas[i].second) / (Mas[i + 1].first - Mas[i].first)));
        else
            buf.push_back(make_pair(Mas[i].first, (Mas[i].second - Mas[i - 1].second) / (Mas[i].first - Mas[i - 1].first)));
    }
    return buf;
}

void DrawGrapic(vector<pair<double, double>> Mas, double r, double g, double b, double size)
{
    glLineWidth(size);
    glBegin(GL_LINE_STRIP);
    glColor3d(r, g, b);
    for (pair<double, double> i: Mas)
    {
        glVertex2d(i.first, i.second);
    }
    glEnd();
}

void DrawFullGrapic(vector<pair<double, double>> MasF, vector<pair<double, double>> MasPF, double size)
{
    int n = MasF.size();
    glLineWidth(size);
    glBegin(GL_LINE_STRIP);
    for (int i = 0; i < n; i++)
    {
        if (MasPF[i].second < 0)
            glColor3d(0, 0, 1);
        else
            glColor3d(1, 0, 0);
        glVertex2d(MasF[i].first, MasF[i].second);
    }
    glEnd();
}

void DrawExtremum(vector<pair<double, double>> Mas, double epsilon)
{

    double Max = Mas[0].second;
    double Min = Mas[0].second;

    for (pair<double, double> i : Mas)
    {
        if (i.second > Max)
            Max = i.second;
        if (i.second < Min)
            Min = i.second;
    }

    int n = Mas.size();
    /*���� ������ ��������� �����������. ��� ���������� ���������� ��������� � Max, Min.
    ���� �� ��������� ��������� �� ����� Max, Min, �� ������ ������� ����, ����� ����� �������.*/
    glPointSize(8);
    for (int i = 0; i < n; i++)
    {
        if ((i != 0) && (i != n - 1))
        {
            if ((Mas[i - 1].second < Mas[i].second) && (Mas[i].second > Mas[i + 1].second))
            {
                if (fabs(Mas[i].second - Max) < epsilon)
                {
                    glColor3d(1, 1, 0);
                    glBegin(GL_POINTS);
                    glVertex2d(Mas[i].first, Mas[i].second);
                    glEnd();
                }
                else
                {
                    glColor3d(0, 1, 1);
                    glBegin(GL_POINTS);
                    glVertex2d(Mas[i].first, Mas[i].second);
                    glEnd();
                }
            }
            else if ((Mas[i - 1].second > Mas[i].second) && (Mas[i].second < Mas[i + 1].second))
            {
                if (fabs(Mas[i].second - Min) < epsilon)
                {
                    glColor3d(1, 1, 0);
                    glBegin(GL_POINTS);
                    glVertex2d(Mas[i].first, Mas[i].second);
                    glEnd();
                }
                else
                {
                    glColor3d(0, 1, 1);
                    glBegin(GL_POINTS);
                    glVertex2d(Mas[i].first, Mas[i].second);
                    glEnd();
                }
            }
        }
        else if (i == 0)
        {
            if (Mas[i].second > Mas[i + 1].second)
            {
                if (fabs(Mas[i].second - Max) < epsilon)
                {
                    glColor3d(1, 1, 0);
                    glBegin(GL_POINTS);
                    glVertex2d(Mas[i].first, Mas[i].second);
                    glEnd();
                }
                else
                {
                    glColor3d(0, 1, 1);
                    glBegin(GL_POINTS);
                    glVertex2d(Mas[i].first, Mas[i].second);
                    glEnd();
                }
            }
            else if (Mas[i].second < Mas[i + 1].second)
            {
                if (fabs(Mas[i].second - Min) < epsilon)
                {
                    glColor3d(1, 1, 0);
                    glBegin(GL_POINTS);
                    glVertex2d(Mas[i].first, Mas[i].second);
                    glEnd();
                }
                else
                {
                    glColor3d(0, 1, 1);
                    glBegin(GL_POINTS);
                    glVertex2d(Mas[i].first, Mas[i].second);
                    glEnd();
                }
            }
        }
        else
        {
            if (Mas[i - 1].second < Mas[i].second)
            {
                if (fabs(Mas[i].second - Max) < epsilon)
                {
                    glColor3d(1, 1, 0);
                    glBegin(GL_POINTS);
                    glVertex2d(Mas[i].first, Mas[i].second);
                    glEnd();
                }
                else
                {
                    glColor3d(0, 1, 1);
                    glBegin(GL_POINTS);
                    glVertex2d(Mas[i].first, Mas[i].second);
                    glEnd();
                }
            }
            else if (Mas[i - 1].second > Mas[i].second)
            {
                if (fabs(Mas[i].second - Min) < epsilon)
                {
                    glColor3d(1, 1, 0);
                    glBegin(GL_POINTS);
                    glVertex2d(Mas[i].first, Mas[i].second);
                    glEnd();
                }
                else
                {
                    glColor3d(0, 1, 1);
                    glBegin(GL_POINTS);
                    glVertex2d(Mas[i].first, Mas[i].second);
                    glEnd();
                }
            }
        }
    }
}

void DrawOSK()
{
    const int sk = 50;

    DrawLine(-sk, 0, 0, sk, 0, 0, 1, 0, 0, 2); //��� X
    DrawLine(0, -sk, 0, 0, sk, 0, 0, 1, 0, 2); //��� Y

    /*����������� �����*/
    for (int i = -sk; i < 0; i++)
    {
        DrawLine(i, -sk, 0, i, sk, 0, 0.5, 0.5, 0.5, 0.1);
        DrawLine(-sk, i, 0, sk, i, 0, 0.5, 0.5, 0.5, 0.1);
    }

    for (int i = 1; i <= sk; i++)
    {
        DrawLine(i, -sk, 0, i, sk, 0, 0.5, 0.5, 0.5, 0.1);
        DrawLine(-sk, i, 0, sk, i, 0, 0.5, 0.5, 0.5, 0.1);
    }
}

void DrawLine(float x1, float y1, float z1, float x2, float y2, float z2, float r, float g, float b, float size)
{
    glLineWidth(size);
    glBegin(GL_LINES);
    glColor3d(r, g, b);
    glVertex3d(x1, y1, z1);
    glVertex3d(x2, y2, z2);
    glEnd();
}