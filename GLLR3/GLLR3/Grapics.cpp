#include "Grapics.h"
#include <iostream>
#include <algorithm>
#include <math.h>
#include "glut.h"
#include "GLAUX.H"
using namespace std;

int CGrapic::accuracy = 10;
double CGrapic::Color::R = 1; double CGrapic::Color::G = 1; double CGrapic::Color::B = 0;

double C2DGrapic::Beg1 = 0; double C2DGrapic::End1 = 0;
CExplisitFunction CGrapic::* Func = NULL;

void C2DGrapic::UseFunction(double B, double E, CExplisitFunction* F)
{
	C2DGrapic::Beg1 = B; C2DGrapic::End1 = E;
	Func = F;
}

void C2DGrapic::DrawGrapic()
{
	int Step = 1 / (double)(accuracy - 1); //������ ���� ���������� �������
	glBegin(GL_LINE_STRIP);
	glColor3d(Color::R, Color::G, Color::B);
	for (double i = Beg1; i <= End1; i += 1 / (double)(accuracy - 1))
	{
		glVertex3d(i, Func->Calculate(i, 0), 0);
	}
}