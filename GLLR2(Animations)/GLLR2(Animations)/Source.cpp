#define _USE_MATH_DEFINES
#include <iostream>
#include <conio.h>
#include <math.h>
#include <Windows.h>
#include <time.h>
#include "glut.h"
#include "GLAUX.H"
#include "Camera.h"
using namespace std;

const double StartTime = GetTickCount64() / 1000.; //��������� �����

#pragma comment (lib, "GLAUX.lib")
void changeSize(int width, int height); //�, ���������� ��� ��������� ����
void renderScene(); //�, ���������� ��� ��������� �����

void DrawLine(float x1, float y1, float z1, float x2, float y2, float z2, float r, float g, float b, float size = 1.0);
void DrawOSK();

//�������������
void DrawQwad(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double r1, double g1, double b1, double r2, double g2, double b2, double r3, double g3, double b3, double r4, double g4, double b4);


//����������� ����
//(x,y,z) - ���������� ������ �����
//radius - ������ �����
//(r,g,b) - ���� ����� ����������
//(r2,g2,b2) - ���� ����� ������
void DrawCircle(double x, double y, double z, double radius, double r, double g, double b, double r2, double g2, double b2);

//���� ������. (x,y,z) - ���������� ������ ������, 
//radius - ������ ������� ���������� ������
//corn - ���� ������� ������
//size - ������ ������
void DrawDuga(double x, double y, double z, double radius, double corn, double r, double g, double b, double size);

//������� ������� ���������
//(x,y,z) - ���������� ������ ����������
//inrad - ������ ����������� ����������
//outrad - ������ �������� ����������
//size - ������ �������
//n - ���-�� ��������
void DrawPropeller(double x, double y, double z, double inrad, double outrad, double r, double g, double b, double size, int n = 3);

int main(int argc, char* argv[])
{
	glutInit(&argc, argv); //�������������� Glut
	glutInitWindowSize(800, 600); //������������� ������� ����
	glutInitWindowPosition(200, 200); //������������� ��������� ��� �������� ������ ���� ������
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE); //������������� ��� ���� RGB - ����� � ������� ������

	int window1 = glutCreateWindow("LR2"); //�������� ���� � ������ LR2 � ��������� ��� handler � window1

    glutReshapeFunc(changeSize); //���������� ������� changeSize, ��� ������� ��� ����������� ����������� ���� ��� ��������� ����
	glutDisplayFunc(renderScene); //���������� ������� renderScene, ��� ������� ��� ��������� ����������� ����
    glutIdleFunc(renderScene);

    C2DCamera::KeyboardMove();
	
	glutMainLoop(); //������ � ������� ���� GLUT
	
	return 0;
}

void changeSize(int width, int height)
{
    C2DCamera::DoGood(width, height, 1, 1, 1);
}

void renderScene()
{   
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    C2DCamera::Look();

    double dt = GetTickCount64() / 1000. - StartTime; //����� � ������� ������� ��������� � ��������

#ifdef OSK
    DrawOSK();
#endif

    //��������
    glPushMatrix();
    glTranslated(0, 0, -0.3);
    //1
    glPushMatrix();
    glRotated(45, 0, 0, 1);
    glRotated((1-cos(M_PI * dt / 10)) * 45, 0, 0, 1); //������� �� ���� (1 - cos(M_PI * dt / 10)) * 45
    DrawCircle(-15, 0, 0, 3, 1, 1, 0, 0, 1, 1);
    DrawQwad(8, 2, 0, 10, 1, 0, 10, -1, 0, 8, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    DrawQwad(-12.5, -0.5, 0, -12.5, 0.5, 0, 8, 0.5, 0, 8, -0.5, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0);
    glPopMatrix();
    //
    //2
    glPushMatrix();
    glRotated(-45, 0, 0, 1);
    glRotated(-(1 - cos(M_PI * dt / 10)) * 45, 0, 0, 1); //������� �� ���� -(1 - cos(M_PI * dt / 10)) * 45
    DrawCircle(15, 0, 0, 3, 0, 1, 1, 1, 1, 0);
    DrawQwad(-8, 2, 0, -10, 1, 0, -10, -1, 0, -8, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    DrawQwad(-8, -0.5, 0, -8, 0.5, 0, 12.5, 0.5, 0, 12.5, -0.5, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1);
    glPopMatrix();
    //
    glPopMatrix();
    //

    //���������
    glPushMatrix();
    glTranslated(0,-9,-0.2);

    glPushMatrix();
    glTranslated(4, 0, 0);
    glRotated(45, 0, 0, 1);
    DrawDuga(0, 0, 0, 3, 180, 1, 0.9, 0, 1);
    glPopMatrix();

    glPushMatrix();
    glTranslated(-4, 0, 0);
    glRotated(-45, 0, 0, 1);
    DrawDuga(0, 0, 0, 3, 180, 1, 0.9, 0, 1);
    glPopMatrix();

    glPushMatrix();
    glTranslated(0, -3, 0);
    DrawQwad(-2, 0, 0, 2, 0, 0, 2, 1, 0, -2, 1, 0, 1, 0.9, 0, 1, 0.9, 0, 1, 0.9, 0, 1, 0.9, 0);
    glPopMatrix();

    glPopMatrix();
    //

    //����������
    //������
    glPushMatrix();
    glRotated(cos(M_PI * dt / 10) * 360, 0, 0, 1); //������� �� ���� cos(M_PI * dt / 10) * 360
    DrawPropeller(0, 0, 0, 4, 2, 0, 1, 1, 0.25, 10);
    glPopMatrix();
    //
    //��������
    glPushMatrix();
    glScaled(1, -1, 1); //"���������" �������������� �� y
    glRotated(cos(M_PI * dt / 10) * 360, 0, 0, 1); //������� �� ���� cos(M_PI * dt / 10) * 360
    DrawPropeller(0, 0, 0.1, 4, 2, 1, 1, 0, 0.25, 10);
    glPopMatrix();
    //
    //������� ����
    DrawCircle(0, 0, -0.1, 6, 0, 0, 0, 1, 0.6, 0);
    //
    //


    glutSwapBuffers(); //�������� �������
}

void DrawLine(float x1, float y1, float z1, float x2, float y2, float z2, float r, float g, float b, float size)
{
    glLineWidth(size);
    glBegin(GL_LINES);
    glColor3d(r, g, b);
    glVertex3d(x1, y1, z1);
    glVertex3d(x2, y2, z2);
    glEnd();
}

void DrawOSK()
{
    const int sk = 50;
    for (int i = -sk; i < 0; i++)
    {
        DrawLine(i, -sk, -1, i, sk, -1, 0.5, 0.5, 0.5, 0.5);
        DrawLine(-sk, i, -1, sk, i, -1, 0.5, 0.5, 0.5, 0.5);
    }

    for (int i = 1; i <= sk; i++)
    {
        DrawLine(i, -sk, -1, i, sk, -1, 0.5, 0.5, 0.5, 0.5);
        DrawLine(-sk, i, -1, sk, i, -1, 0.5, 0.5, 0.5, 0.5);
    }

    DrawLine(-sk, 0, -0.5, sk, 0, -0.5, 1, 0, 0, 2); //��� X
    DrawLine(0, -sk, -0.5, 0, sk, -0.5, 0, 1, 0, 2); //��� Y
}

void DrawQwad(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double r1, double g1, double b1, double r2, double g2, double b2, double r3, double g3, double b3, double r4, double g4, double b4)
{
    glBegin(GL_QUADS);
    glColor3d(r1, g1, b1);
    glVertex3d(x1, y1, z1);
    glColor3d(r2, g2, b2);
    glVertex3d(x2, y2, z2);
    glColor3d(r3, g3, b3);
    glVertex3d(x3, y3, z3);
    glColor3d(r4, g4, b4);
    glVertex3d(x4, y4, z4);
    glEnd();
}

void DrawCircle(double x, double y, double z, double radius, double r, double g, double b, double r2, double g2, double b2)
{
    int n = 100;//���������� �����
    glBegin(GL_TRIANGLE_FAN);
    glColor3d(r2, g2, b2);
    glVertex3d(x, y, z);
    glColor3d(r, g, b);
    for (int i = 0; i <= n; i++)
        glVertex3d(x + radius * cos(i * 2 * M_PI / n),
            y + radius * sin(i * 2 * M_PI / n), 0);
    glEnd();
}

void DrawDuga(double x, double y, double z, double radius, double corn, double r, double g, double b, double size)
{
    const int exact = 100; //���-�� �������� � ����

    glBegin(GL_QUAD_STRIP);
    glColor3d(r, g, b);
    for (int i = 0; i <= exact; i++)
    {
        double bufcorn = i * (corn * M_PI / 180) / exact;
        glVertex3d(x + radius * cos(bufcorn), y + radius * sin(bufcorn), z); //����� ���������� ����
        glVertex3d(x + (radius + size) * cos(bufcorn), y + (radius + size) * sin(bufcorn), z); //����� ������� ����
    }
    glEnd();
}

void DrawPropeller(double x, double y, double z, double inrad, double outrad, double r, double g, double b, double size, int n)
{
    glMatrixMode(GL_MODELVIEW);
    for (int i = 0; i < n; i++)
    {
        glPushMatrix();
        glRotated(i * 360 / n, 0, 0, 1); //������� ������ �������
        DrawDuga(x + inrad / 2, y, z, (inrad / 2) - size, 180, r, g, b, size); //���������� �������
        glScaled(1, -1, 1); //��������� �� ��� X ��� ������� �������
        DrawDuga(x + inrad - size + outrad / 2, y, z, (outrad / 2) - size, 180, r, g, b, size); //������� �������
        glPopMatrix();
    }
    DrawCircle(x, y, z + 0.1, size, r, g, b, 1 - r, 1 - g, 1 - b);
}