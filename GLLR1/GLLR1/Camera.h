#pragma once

#include "Freeglut/glut.h"
#include "GLAUX/GLAUX.H"

using namespace std;

class C2DCamera
{
	GLdouble Eye[3];
	GLint ZFar;
	GLint ZNear;
	static constexpr GLdouble MoveSpeed = 0.1;

	void UpdOrtoPerspective(); //������������� ��������� ���������, � ����������� �� ��������� ������
	void SetEye(GLdouble NewEye[3]);

public:
	GLint GiveZFar()
	{
		return ZFar;
	}
	GLint GiveZNear()
	{
		return ZNear;
	}

	C2DCamera(); //�����������, ��������������� ������ � (0,0,1) ����������� �� (0,0,0) � ������������ y (0,1,0)
	C2DCamera(GLdouble NewEye[3]); //�����������, ��������������� ������ � Eye ����������� ����� Eye � ������������ y (0,1,0)

	void Move(GLdouble NewEye[3] = NULL); //����������� ������ � ��������� NewEye
	void KeyboardMove(); //����������� ������ c ������� ���������� (w,a,s,d,q,e - �����, �����, ����, ������, ����� � ����� ��������������)
	//�������� ��� ������� ����������� �������
	//friend void PutKeys(unsigned char key, int y, int x);
	friend void CALLBACK Up(void);
	friend void CALLBACK Left(void);
	friend void CALLBACK Down(void);
	friend void CALLBACK Right(void);
	friend void CALLBACK Forvard(void);
	friend void CALLBACK Back(void);
};
