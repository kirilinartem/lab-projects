
#include "Freeglut/glut.h"
#include "GLAUX/GLAUX.H"

#include "Camera.h"

using namespace std;

#pragma comment(lib, "GLAUX.lib")
#pragma comment(lib, "freeglut.lib")

GLdouble EyeX, EyeY, EyeZ;

void C2DCamera::UpdOrtoPerspective()
{
	ZFar = Eye[2] + 1;
	ZNear = Eye[2] - 1;
}

void C2DCamera::SetEye(GLdouble NewEye[3])
{
	Eye[0] = NewEye[0];
	Eye[1] = NewEye[1];
	Eye[2] = NewEye[2];
	UpdOrtoPerspective();
}

C2DCamera::C2DCamera()
{
	GLdouble NewEye[3] = { 0.0, 0.0, 1.0 };
	SetEye(NewEye);
	gluLookAt(Eye[0], Eye[1], Eye[2], Eye[0], Eye[1], Eye[2] - 1, 0, 1, 0);
}

C2DCamera::C2DCamera(GLdouble NewEye[3])
{
	SetEye(NewEye);
	gluLookAt(Eye[0], Eye[1], Eye[2], Eye[0], Eye[1], Eye[2] - 1, 0, 1, 0);
}

void C2DCamera::Move(GLdouble NewEye[3])
{
	if (NewEye == NULL)
	{
		gluLookAt(Eye[0], Eye[1], Eye[2], Eye[0], Eye[1], Eye[2] - 1, 0, 1, 0);
		return;
	}
	SetEye(NewEye);
	gluLookAt(Eye[0], Eye[1], Eye[2], Eye[0], Eye[1], Eye[2] - 1, 0, 1, 0);
}

//void PutKeys(unsigned char key, int y, int x)
//{
//	switch (key)
//	{
//	case 'w': EyeY += C2DCamera::MoveSpeed; break;
//	case 'a': EyeX -= C2DCamera::MoveSpeed; break;
//	case 's': EyeY -= C2DCamera::MoveSpeed; break;
//	case 'd': EyeX += C2DCamera::MoveSpeed; break;
//	case 'q': if (EyeZ > 1) EyeZ -= C2DCamera::MoveSpeed; break;
//	case 'e': EyeX -= C2DCamera::MoveSpeed; break;
//	default:
//		break;
//	}
//}

void CALLBACK Up(void)
{

}
void CALLBACK Left(void)
{

}
void CALLBACK Down(void)
{

}
void CALLBACK Right(void)
{

}
void CALLBACK Forvard(void)
{

}
void CALLBACK Back(void)
{

}

void C2DCamera::KeyboardMove()
{
	EyeX = Eye[0]; EyeY = Eye[1]; EyeZ = Eye[2];
	/*glutKeyboardFunc(PutKeys);*/
	auxKeyFunc(AUX_w, Up);
	auxKeyFunc(AUX_s, Left);
	auxKeyFunc(AUX_a, Down);
	auxKeyFunc(AUX_d, Right);
	auxKeyFunc(AUX_q, Forvard);
	auxKeyFunc(AUX_e, Back);
	GLdouble NewEye[3] = { EyeX, EyeY, EyeZ };
	Move(NewEye);
}

