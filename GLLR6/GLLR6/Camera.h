#pragma once

#ifndef Camera_on
#define Camera_on
using namespace std;

class C2DCamera
{
	//����������� ����������, ������������ ������� ����
	static int Width;
	static int Height;
	//����������� ����������, ���������� �� ������� ������
	static GLdouble Eye[3];
	static GLint ZFar;
	static GLint ZNear;
	static constexpr GLdouble MoveSpeed = 1;
	static C2DCamera* AdrCurrCam; //������ ������������ ������
	//���������� ���������� �� ������ ���������� �����
	GLdouble BufEye[3];
	//

	static void UpdOrtoPerspective(); //������������� ��������� ���������, � ����������� �� ��������� ������


public:
	C2DCamera(); //�����������, ��������������� ������ � (0,0,10) ����������� �� (0,0,0) � ������������ y (0,1,0)
	C2DCamera(GLdouble NewEye[3]); //�����������, ��������������� ������ � Eye ����������� ����� Eye � ������������ y (0,1,0)
	
	///Get&Set
	static int GetWidth()
	{
		return Width;
	}
	static int GetHeight()
	{
		return Height;
	}
	static GLint GetZFar()
	{
		return ZFar;
	}
	static GLint GetZNear()
	{
		return ZNear;
	}
	static GLdouble GetEyeX()
	{
		return Eye[0];
	}
	static GLdouble GetEyeY()
	{
		return Eye[1];
	}
	static GLdouble GetEyeZ()
	{
		return Eye[2];
	}
	friend void PutKeys(unsigned char key, int y, int x); //�������� ��� ������� ����������� �������
	///

	void UseCamera(); //������������ ������������ ������

	static void SetEye(GLdouble NewEye[3]);
	
	static void Move(GLdouble NewEye[3]); //����������� ������ � ��������� NewEye
	static void Look(); //�������� ��������� ������ �������� Eye
	static void KeyboardMove(); //����������� ������ c ������� ���������� (w,a,s,d,q,e - �����, �����, ����, ������, ����� � ����� ��������������)

	static void DoGood(int width, int height, double r, double g, double b); //�������, ������� ���� ������� � ������� ��������� ������� ������ � ������� ������ ������ (�������� � ���� ������ �������, ������� ������ ���� ���)
};

#endif
