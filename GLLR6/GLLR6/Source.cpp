#define _USE_MATH_DEFINES
#include <iostream>
#include <conio.h>
#include <math.h>
#include <Windows.h>
#include "glut.h"
#include "GLAUX.H"
#include "Camera.h"
#include <vector>
#include <time.h>
using namespace std;

const double StartTime = GetTickCount64(); //��������� �����

void changeSize(int width, int height); //�, ���������� ��� ��������� ����
void renderScene(); //�, ���������� ��� ��������� �����

/*����������� ������� ��������� ��������*/
void DefalautLeaves(double r, double g, double b, double size);

/*����������� ������� ������ ��������
red1, green1, blue1 - ���� ������� �������� ������
red2, green2, blue2 - ���� ��������� ��������� ������
size - ������ ������� ��������
angle1 - ���� ������ �������� �� ��������� ������
angle2 - ���� ������� �������� �� ��������� ������
k - ��������� ���������� �������
leavelvl - �������, ������� � �������� �������� ����� ������
LeaveFunc - ������� ��������� �������
*/
void PifagorTree(int lvl, double red1, double green1, double blue1, double red2, double green2, double blue2, double size, double angle1, double angle2, double k, int leavelvl = -1, void (*LeaveFunc)(double, double, double, double) = DefalautLeaves);

/*������� ������������ ������� ���������*/
void DrawOSK();

/*������� ������������ �����*/
void DrawLine(float x1, float y1, float z1, float x2, float y2, float z2, float r, float g, float b, float size = 1.0);

int main(int argc, char* argv[])
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    glutInit(&argc, argv); //�������������� Glut
    glutInitWindowSize(1600, 1200); //������������� ������� ����
    glutInitWindowPosition(200, 200); //������������� ��������� ��� �������� ������ ���� ������
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE); //������������� ��� ���� RGB - ����� � ������� ������

    int window1 = glutCreateWindow("LR2"); //�������� ���� � ������ LR2 � ��������� ��� handler � window1

    glutReshapeFunc(changeSize); //���������� ������� changeSize, ��� ������� ��� ����������� ����������� ���� ��� ��������� ����
    glutDisplayFunc(renderScene); //���������� ������� renderScene, ��� ������� ��� ��������� ����������� ����
    glutIdleFunc(renderScene);

    C2DCamera::KeyboardMove();

    glutMainLoop(); //������ � ������� ���� GLUT

    return 0;
}

void changeSize(int width, int height)
{
    C2DCamera::DoGood(width, height, 0, 0, 0);
}

void renderScene()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    C2DCamera::Look();

    double dt = (GetTickCount64() - StartTime)/1000.; //����� � ������� ������� ���������
    double dfi = 10*(sin(dt/20) + sin(3 * dt /20) + sin(6 * dt / 20)); //���������� ���� � �������� �������

    PifagorTree(10, 0.6, 0.3, 0, 0, 1, 0, 3, 30 - dfi, 30 + dfi, 0.8, 7);

#ifdef OSK
    DrawOSK();
#endif // OSK

    glutSwapBuffers(); //�������� �������
}

void PifagorTree(int lvl, double red1, double green1, double blue1, double red2, double green2, double blue2, double size, double angle1, double angle2, double k, int leavelvl, void (*LeaveFunc)(double,double,double,double))
{
    glMatrixMode(GL_MODELVIEW);
    int newlvl = lvl - 1;
    if (lvl == 0)
        return;
    if (lvl <= leavelvl)
        LeaveFunc(red2, green2, blue2, size);

    double dr, dg, db;
    dr = (red2 - red1) / (lvl - 1); dg = (green2 - green1) / (lvl - 1); db = (blue2 - blue1) / (lvl - 1);

    DrawLine(0, 0, 0, 0, size, 0, red1, green1, blue1);

    glPushMatrix();
    glTranslated(0, size, 0);
    glScalef(k, k, 1);
    glRotated(angle1, 0, 0, 1);
    PifagorTree(newlvl, red1 + dr, green1 + dg, blue1 + db, red2, green2, blue2, size, angle1, angle2, k, leavelvl, LeaveFunc);
    glPopMatrix();

    glPushMatrix();
    glTranslated(0, size, 0);
    glScalef(k, k, 1);
    glRotated(-angle2, 0, 0, 1);
    PifagorTree(newlvl, red1 + dr, green1 + dg, blue1 + db, red2, green2, blue2, size, angle1, angle2, k, leavelvl, LeaveFunc);
    glPopMatrix();

}

void DefalautLeaves(double r, double g, double b, double size)
{
    glColor3d(r, g, b);
    glBegin(GL_TRIANGLES);
    glVertex2d(0, size);
    glVertex2d(0.5, 0.7 * size);
    glVertex2d(-0.5, 0.7 * size);
    glEnd();
}

void DrawOSK()
{
    const int sk = 50;

    DrawLine(-sk, 0, 0, sk, 0, 0, 1, 0, 0, 2); //��� X
    DrawLine(0, -sk, 0, 0, sk, 0, 0, 1, 0, 2); //��� Y

    /*����������� �����*/
    for (int i = -sk; i < 0; i++)
    {
        DrawLine(i, -sk, 0, i, sk, 0, 0.5, 0.5, 0.5, 0.1);
        DrawLine(-sk, i, 0, sk, i, 0, 0.5, 0.5, 0.5, 0.1);
    }

    for (int i = 1; i <= sk; i++)
    {
        DrawLine(i, -sk, 0, i, sk, 0, 0.5, 0.5, 0.5, 0.1);
        DrawLine(-sk, i, 0, sk, i, 0, 0.5, 0.5, 0.5, 0.1);
    }
}

void DrawLine(float x1, float y1, float z1, float x2, float y2, float z2, float r, float g, float b, float size)
{
    glLineWidth(size);
    glBegin(GL_LINES);
    glColor3d(r, g, b);
    glVertex3d(x1, y1, z1);
    glVertex3d(x2, y2, z2);
    glEnd();
}