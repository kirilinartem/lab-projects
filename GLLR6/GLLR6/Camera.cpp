#include "glut.h"
#include "GLAUX.H"
#include "Camera.h"
#include <iostream>

using namespace std;

int C2DCamera::Width = 0;
int C2DCamera::Height = 0;
GLdouble C2DCamera::Eye[3] = { 0,0,20 };
GLint C2DCamera::ZFar = C2DCamera::Eye[2] + 1;
GLint C2DCamera::ZNear = C2DCamera::Eye[2] - 1;
C2DCamera* C2DCamera::AdrCurrCam = NULL;

void C2DCamera::UpdOrtoPerspective()
{
	ZFar = Eye[2] + 10;
	ZNear = Eye[2] - 10;
}

void C2DCamera::SetEye(GLdouble NewEye[3])
{
	Eye[0] = NewEye[0];
	Eye[1] = NewEye[1];
	Eye[2] = NewEye[2];
	UpdOrtoPerspective();
	return;
}

void C2DCamera::UseCamera()
{
	if (AdrCurrCam != NULL) //���� ���� ����� ������� ������, �� �������������� ��������� ���������� ������ �������� ����������
	{
		AdrCurrCam->BufEye[0] = Eye[0];
		AdrCurrCam->BufEye[1] = Eye[1];
		AdrCurrCam->BufEye[2] = Eye[2];
	}
	//���������� � ������� ������, ������ �� ��������� ������, ������� ����� ������������
	Eye[0] = BufEye[0];
	Eye[1] = BufEye[1];
	Eye[2] = BufEye[2];

}

C2DCamera::C2DCamera()
{
	BufEye[0] = 0.0;
	BufEye[1] = 0.0;
	BufEye[2] = 10.0;
}

C2DCamera::C2DCamera(GLdouble NewEye[3])
{
	BufEye[0] = NewEye[0];
	BufEye[1] = NewEye[1];
	BufEye[2] = NewEye[2];
}

void C2DCamera::Look()
{
	gluLookAt(Eye[0], Eye[1], Eye[2], Eye[0], Eye[1], Eye[2] - 1, 0, 1, 0);
}

void C2DCamera::Move(GLdouble NewEye[3])
{
	SetEye(NewEye);
	Look();
}

void PutKeys(unsigned char key, int y, int x)
{
	switch (key)
	{
	case (unsigned char)'w': C2DCamera::Eye[1] += C2DCamera::MoveSpeed; break;
	case (unsigned char)'a': C2DCamera::Eye[0] -= C2DCamera::MoveSpeed; break;
	case (unsigned char)'s': C2DCamera::Eye[1] -= C2DCamera::MoveSpeed; break;
	case (unsigned char)'d': C2DCamera::Eye[0] += C2DCamera::MoveSpeed; break;
	case (unsigned char)'q': 
		if (C2DCamera::Eye[2] > 1) C2DCamera::Eye[2] -= C2DCamera::MoveSpeed; 
		C2DCamera::UpdOrtoPerspective(); 
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(45, C2DCamera::GetWidth() / C2DCamera::GetHeight(), C2DCamera::GetZNear(), C2DCamera::GetZFar()); break;
	case (unsigned char)'e': 
		C2DCamera::Eye[2] += C2DCamera::MoveSpeed; 
		C2DCamera::UpdOrtoPerspective();
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(45, C2DCamera::GetWidth() / C2DCamera::GetHeight(), C2DCamera::GetZNear(), C2DCamera::GetZFar()); break;
	default:
		break;
	}
}

void C2DCamera::KeyboardMove()
{
	glutKeyboardFunc(PutKeys);
}

void C2DCamera::DoGood(int width, int height, double r, double g, double b)
{
	Width = width;
	Height = height;

	glViewport(0, 0, Width, Height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45, Width / Height, ZNear, ZFar);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	C2DCamera::Look();

	glClearColor(r, g, b, 0);

	glEnable(GL_DEPTH_TEST); //�������� ���� �������
}