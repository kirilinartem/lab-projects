#include "Functions.h"
#include <iostream>
#include <algorithm>
#include <math.h>
using namespace std;

//CExplisitFunction
CExplisitFunction::CExplisitFunction(int Sig, CExplisitFunction* F, CExplisitFunction* S, double P)
{
	First = F;
	Second = S;

	F->Parent = this;
	S->Parent = this;

	Sign = Sig;
	Param = P;
}

double CExplisitFunction::Calculate(double x, double y)
{
	double buf1, buf2;
	if (this->First != NULL)
		buf1 = this->First->Calculate(x, y);
	if (this->Second != NULL)
		buf2 = this->Second->Calculate(x, y);
	switch (this->Sign)
	{
	case 101:
		return (1 - buf1); break;
	case 102:
		return max(buf1, buf2); break;
	case 103:
		return min(buf1, buf2); break;
	case 1041:
		return min(buf1, 1 - buf2); break;
	case 1042:
		return buf1 - min(buf1, buf2); break;
	case 105:
		return min(max(buf1, 1 - buf2), max(1 - buf1, buf2)); break;
	case 106:
		return buf1 * buf1; break;
	case 107:
		return sqrt(buf1); break;
	case 108:
		if (buf1 < 0.5)
			return pow(2, Param - 1) * pow(buf1, Param);
		else
			return 1 - pow(2, Param - 1) * pow(1 - buf1, Param);
		break;
	case 109:
		if (buf1 < 0.5)
			return 1 - pow(2, Param - 1) * pow(1 - buf1, Param);
		else
			return pow(2, Param - 1) * pow(buf1, Param);
		break;
	case 110:
		if (buf1 >= Param)
			return buf1;
		else
			return 0;
	default:
		break;
	}
}
//

//CExplisitFunction1A
CExplisitFunction1A::CExplisitFunction1A(double (*NewFunc)(double x), bool Arg)
{
	Func = NewFunc;
	Argument = Arg;
}

double CExplisitFunction1A::Calculate(double x, double y)
{
	if (Argument == 1)
		return Func(x);
	else
		return Func(y);
}
//

//CExplisitFunction2A
CExplisitFunction2A::CExplisitFunction2A(double (*NewFunc)(double x, double y))
{
	Func = NewFunc;
}

double CExplisitFunction2A::Calculate(double x, double y)
{
	return Func(x, y);
}
//
