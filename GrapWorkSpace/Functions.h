#pragma once
#include <cstddef>

#ifndef Functions_on
#define Functions_on
using namespace std;

class CExplisitFunction
{
protected:
	CExplisitFunction* Parent; //������ �� �������� ��������� (������������ ���� ������)
	CExplisitFunction* First; //������ ������� �������� (����-������� 1)
	CExplisitFunction* Second; //������ ������� �������� (����-������� 2)
	int Sign; //����� �������� (=0 ���� ���� ������)
	double Param; //��������, ����������� � �������� (=0 �� ���������)

	CExplisitFunction()
	{
		Parent = NULL; First = NULL; Second = NULL; Sign = 0; Param = 0;
	}
public:
	CExplisitFunction(int Sig, CExplisitFunction* F, CExplisitFunction* S, double P = 0); //����������� ��� ������� ������� ������������ �������� (� - ����� ��������, F - ������ �������, S - ������ �������)

	virtual double Calculate(double x, double y); //�����, ����������� �������� ������������� ������ ��� ��������� (x,y)

	///�������� (������� ������ ������ CExplisitFunction � ������ ������������, ���� �������� ����� ������� � ������ ��������� � ��� �������)

	//�������������� ��������

	//�������� ��� ��������� �����������
	friend CExplisitFunction* Compl(CExplisitFunction* Func) //����������
	{
		return &CExplisitFunction(101, Func, NULL);
	}
	friend CExplisitFunction* Conj(CExplisitFunction* Func1, CExplisitFunction* Func2) //�����������
	{
		return &CExplisitFunction(102, Func1, Func2);
	}
	friend CExplisitFunction* Disj(CExplisitFunction* Func1, CExplisitFunction* Func2) //�����������
	{
		return &CExplisitFunction(103, Func1, Func2);
	}
	friend CExplisitFunction* Diff(CExplisitFunction* Func1, CExplisitFunction* Func2) //�������� (min(A,1-B))
	{
		return &CExplisitFunction(1041, Func1, Func2);
	}
	friend CExplisitFunction* Diff2(CExplisitFunction* Func1, CExplisitFunction* Func2) //�������� (A - Disj(A,B))
	{
		return &CExplisitFunction(1042, Func1, Func2);
	}
	friend CExplisitFunction* DisjAdd(CExplisitFunction* Func1, CExplisitFunction* Func2) //������������� �����
	{
		return &CExplisitFunction(105, Func1, Func2);
	}
	friend CExplisitFunction* Con(CExplisitFunction* Func) //����������������
	{
		return &CExplisitFunction(106, Func, NULL);
	}
	friend CExplisitFunction* Dil(CExplisitFunction* Func) //����������
	{
		return &CExplisitFunction(107, Func, NULL);
	}
	friend CExplisitFunction* Int(CExplisitFunction* Func, int n) //��������� ������������� (�� �������� n)
	{
		return &CExplisitFunction(108, Func, NULL, n);
	}
	friend CExplisitFunction* Blr(CExplisitFunction* Func) //��������� ������������� (�� �������� n)
	{
		return &CExplisitFunction(109, Func, NULL, n);
	}
	friend CExplisitFunction* AlphaLvl(CExplisitFunction* Func, double Alpha)
	{
		return &CExplisitFunction(110, Func, NULL, Alpha); //������ �����-������
	}

	~CExplisitFunction() {};
};

class CExplisitFunction1A :CExplisitFunction
{
private:
	double (*Func)(double x); //������ �� ��������� �������
	bool Argument; //����, � ������ 3 ������ �������, ���������� x(1) ��� y(0) ���������� � �������
public:
	CExplisitFunction1A(double (*NewFunc)(double x), bool Arg = 1); //����������� ��� ������� ��������� ������� (NewFunc - �������, Arg - ������ ������� ��������)

	virtual double Calculate(double x, double y); //�����, ����������� �������� ������������� ������ ��� ��������� (x,y)

	~CExplisitFunction1A() {};
};

class CExplisitFunction2A :CExplisitFunction
{
private:
	double (*Func)(double x, double y);
public:
	CExplisitFunction2A(double (*NewFunc)(double x, double y)); //����������� ��� ������� ��������� ������� (NewFunc - �������, Arg - ������ ������� ��������)

	virtual double Calculate(double x, double y); //�����, ����������� �������� ������������� ������ ��� ��������� (x,y)

	~CExplisitFunction2A() {};
};
#endif