#pragma once
#include <vector>
#include "Functions.h"

#ifndef Grapics_on
#define Grapics_on
using namespace std;

class CGrapic
{
protected:
	static int accuracy; //���-�� ��������� �� ���������� [n;n+1]
	vector<double>;
	static struct Color //���� �������
	{
		static double R, G, B;
	};
public:
	///Set&Get
	static void SetAccur(int accur)
	{
		accuracy = accur;
	}
	static void SetCollor(double r, double g, double b)
	{
		Color::R = r; Color::G = g; Color::B = b;
	}

};
class C2DGrapic :CGrapic
{
private:
	static double Beg1, End1; //������ � ����� ��������, �� ������� ����� ������������ ������� 
	static CExplisitFunction *Func; //������ �� �������������� �������
public:
	static void UseFunction(double B, double E, CExplisitFunction* F); //���������, ��� ������� ����� ���������� �� [B, E]
	static void DrawGrapic();
};
#endif
