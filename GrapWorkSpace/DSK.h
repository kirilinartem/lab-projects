#pragma once
#include "glut.h"
#include "GLAUX.H"

#ifndef DSK_on
#define DSK_on
using namespace std;

class CDSK
{
private:
	static struct AxisColour
	{
		static struct X
		{
			static GLdouble Red, Green, Blue; //���� ��� X
		};
		static struct Y
		{
			static GLdouble Red, Green, Blue; //���� ��� Y
		};
	};
	static struct NetColour
	{
		static GLdouble Red, Green, Blue; //���� �����
	};
	

public:
	///������������ � �����������
	
	///Get&Set

	//������������� ���� ��� X
	static void SetXColourd(GLdouble R, GLdouble G, GLdouble B) 
	{
		AxisColour::X::Red = R; AxisColour::X::Green = G; AxisColour::X::Blue = B;
	}
	//������������� ���� ��� Y
	static void SetYColourd(GLdouble R, GLdouble G, GLdouble B)
	{
		AxisColour::Y::Red = R; AxisColour::Y::Green = G; AxisColour::Y::Blue = B;
	}
	//������������� ���� �����
	static void SetNetColourd(GLdouble R, GLdouble G, GLdouble B)
	{
		NetColour::Red = R; NetColour::Green = G; NetColour::Blue = B;
	}
	/*
	������������� ����������� ���� ��� (��-�������, Oy-������, Oz-�����)*/


	///������
	void DravDSK(); //��������� ������� ��������� ���
};

#endif